<?php

namespace Rising\Library;

use Cache;
use Carbon\Carbon;
use Auth, Log, Request, File, DB;
use Form, PDF;
use Webklex\IMAP\Client;

define("PIXER_QUALITY",90);
define("PIXER_ETAT",'on');
define("PIXER_FILTER",'filtre1');
define("PIXER_TAG",'off');
define("PIXER_TAG_POS",8);
define("PIXER_TAG_SCALE",700);
define("PIXER_TAG_FILE","tag_koaci.png");

class Library
{
    public static function nanoboard_navgroup($title, $icon, $navs) {

        $route = Request::route();
        $currentRouteName = $route->getName();
        $visibleSubMenu = in_array($currentRouteName, $navs);

        $html = '<div class="BlockUserItem"><div class="ic"><img src="'.$icon.'" class="BlockUserItem-icon" alt="" /></div><div class="AlignLeft item-menu">'.$title.'</div></div>
        <div class="BlockUsersubmenu" style="'. $visibleSubMenu === true ? 'display:block' : 'display:none'.'">';
        foreach($navs as $nav_route => $nav_label) {
            $html .= '<a href="'.route($nav_route).'"><div class="BlockUserItemsub"'.$nav_label.'</div></a>';
        }
        $html .= '</div>';
        
        return $html;
    }

    public static function stripAccents($str) {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    /**
     * Escape special characters for a LIKE query.
     *
     * @param string $value
     * @param string $char
     *
     * @return string
     */
    function escape_like(string $value, string $char = '\\'): string
    {
        return str_replace(
            [$char, '%', '_'],
            [$char.$char, $char.'%', $char.'_'],
            $value
        );
    }

    public function nouveauCode($table, $idColumn, $length, $prefix = '', $whereClause=null, $dateColumn=null) {
        $date = '';
        $lengthPrefix = strlen($prefix) + strlen($date) + 1;
        $nvCode = DB::table($table)->select(DB::raw('IFNULL(MAX(SUBSTRING('.$idColumn.', '.$lengthPrefix.', '.$length.')) + 1, \'1\') AS CODE'));
        if ($whereClause != null) {
            $nvCode = $nvCode->whereRaw($whereClause);
        }
        if ($dateColumn != null) {
            $NOW = Carbon::now();
            $date = date('ym');
            $nvCode = $nvCode->where(DB::raw('YEAR('.$dateColumn.')'), '=', $NOW->year)->where(DB::raw('MONTH('.$dateColumn.')'), '=', $NOW->month);
        }
        $nvCode = $nvCode->first();
        $length = $length - $lengthPrefix + 1;
        $code = $prefix.$date.str_pad($nvCode->CODE, $length, '0', STR_PAD_LEFT);
        return $code;
    }

    public function sanitize_amount($amount) {
        $amount = trim(preg_replace('/\s/','', $amount));
        $amount = trim(preg_replace('/(,)+/','.', $amount));
        return $amount;
    }

    public function downloadPhoto($url, $type, $resizing=true, $square=0, $keep=0) {
        $public_path = base_path().'/../assets';

        $public_path = $public_path.'/'.$type.'/thumbnails';
        if (!is_dir($public_path)) {
            mkdir($public_path, 0777, TRUE);
        }

        $file_name = basename($url);
        $extension = 'jpg';
        $file_name = "photo_".time().'.'.$extension;
        $complete_save_loc = $public_path .'/'.$file_name;
        file_put_contents($complete_save_loc, file_get_contents($url));

        $dir_date = date('Y/m');
        if ($resizing == true) {
            $thumbs = config('library.thumbs.'.$type);
            foreach ($thumbs as $key => $size) {
                $target = $public_path . "/" . $size . '/' . $dir_date;
                if (!is_dir($target)) {
                    mkdir($target, 0777, TRUE);
                }
                $tag_on = $size > 600 ? 'on' : 'off';
                Library::resize($public_path . "/" . $file_name, $target . '/' . $file_name, $size, 90, $extension, 0, 0, $tag_on, $square);
            }
        }

        //if ($keep == 0) {
        File::delete($public_path."/".$file_name);
        //}

        return $dir_date.'/'.$file_name;

    }

    public function uploadPhoto($f, $type, $resizing=true, $square=0, $keep=0) {
        $public_path = public_path().'/assets';

        $public_path = $public_path.'/'.$type;
        if (!is_dir($public_path)) {
            mkdir($public_path, 0777, TRUE);
        }

        $extension = $f->getClientOriginalExtension();
        $file_name = "photo_".uniqid (rand(), true).'.'.$extension;

        $dir_date = date('Y/m');
        $f->move($public_path, $file_name);

        $taggable = false;
        // VERIFIE SI L'IMAGE DOIT ETRE TAGGé (SIZE > 600)
        $thumbs = config('library.thumbs.'.$type);
        /*foreach ($thumbs as $key => $size) {
            if ($size > 600) {
                $taggable = true;
                break;
            }
        }*/
        // ************************************************

        // ON REDIMENSIONNE L'IMAGE A 2000 POUR LE TAG
        $big_file = $public_path.'/tmp/'.$file_name;
        if ($taggable == true) {
            if (!is_dir($public_path . '/tmp')) {
                mkdir($public_path . '/tmp', 0777, TRUE);
            }
            Library::resize($public_path . "/" . $file_name, $big_file, 2000, 90, $extension, 0, 0, 'off', '');
        }
        // ************************************************

        if ($resizing == true) {
            foreach ($thumbs as $key => $size) {
                $target = $public_path."/".$size;
                if (!is_dir($target)) {
                    mkdir($target, 0777, TRUE);
                }
                //$tag_on = $size > 600 ? 'on' : 'off';
                $tag_on = 'off';
                if ($tag_on == 'on') {
                    Library::resize($big_file,$target.'/'.$file_name,$size,90,$extension,0,0,$tag_on,$square);
                } else {
                    Library::resize($public_path."/".$file_name,$target.'/'.$file_name,$size,90,$extension,0,0,$tag_on,$square);
                }

            }
        }

        //if ($keep == 0) {
        if ($taggable == true) {
            File::delete($big_file);
        }
        File::delete($public_path."/".$file_name);
        //}

        return $file_name;
    }

    /*
     * @param string $filepath Chemin du fichier à uploader
     * @param string $type Type déclaré dans la config library.php
     * @param boolean $resizing
     * @param int $square
     * @param int $keep
     *
     * @return string
     */
    public function uploadPhotoFromPath($filepath, $type, $resizing=true, $square=0, $keep=0) {
        $public_path = base_path().'/../assets';

        $public_path = $public_path.'/'.$type.'/thumbnails';
        if (!is_dir($public_path)) {
            mkdir($public_path, 0777, TRUE);
        }

        $f = new \Symfony\Component\HttpFoundation\File\File($filepath);

        $extension = $f->getExtension();
        $file_name = $f->getFilename();

        $dir_date = date('Y/m');
        $f->move($public_path, $file_name);

        $taggable = false;
        // VERIFIE SI L'IMAGE DOIT ETRE TAGGé (SIZE > 600)
        $thumbs = config('library.thumbs.'.$type);
        foreach ($thumbs as $key => $size) {
            if ($size > 600) {
                $taggable = true;
                break;
            }
        }
        // ************************************************

        // ON REDIMENSIONNE L'IMAGE A 2000 POUR LE TAG
        $big_file = $public_path.'/tmp/'.$file_name;
        if ($taggable == true) {
            if (!is_dir($public_path . '/tmp')) {
                mkdir($public_path . '/tmp', 0777, TRUE);
            }
            Library::resize($public_path . "/" . $file_name, $big_file, 2000, 90, $extension, 0, 0, 'off', '');
        }
        // ************************************************

        if ($resizing == true) {
            foreach ($thumbs as $key => $size) {
                $target = $public_path."/".$size.'/'.$dir_date;
                if (!is_dir($target)) {
                    mkdir($target, 0777, TRUE);
                }
                $tag_on = $size > 600 ? 'on' : 'off';
                if ($tag_on == 'on') {
                    Library::resize($big_file,$target.'/'.$file_name,$size,90,$extension,0,0,$tag_on,$square);
                } else {
                    Library::resize($public_path."/".$file_name,$target.'/'.$file_name,$size,90,$extension,0,0,$tag_on,$square);
                }
            }
        }

        //if ($keep == 0) {
        if ($taggable == true) {
            File::delete($big_file);
        }
        File::delete($public_path."/".$file_name);
        //}

        return $dir_date.'/'.$file_name;
    }

    public function deletePhoto($filename, $type, $resized=true) {
        $public_path = public_path().'/assets';

        $public_path = $public_path.'/'.$type.'/thumbnails';
        if (!is_dir($public_path)) {
            mkdir($public_path, 0777, TRUE);
        }

        if ($resized == true) {
            $thumbs = config('library.thumbs.'.$type);
            foreach ($thumbs as $key => $size) {
                $target = $public_path.'/'.$size;
                if(file_exists($target."/".$filename)) {
                    $result = unlink($target."/".$filename);

                }

                //File::delete($target."/".$filename);
            }
        }
        File::delete(base_path().'/thumbs/'.$filename);
    }

    public function uploadFile($f, $type='', $subdirectory="") {
        $path = public_path()."/assets/".$type;
        if (!is_dir($path)) {
            mkdir($path);
        }
        if ($subdirectory != '') {
            $path .= '/'.$subdirectory;
        }
        $extension = $f->getClientOriginalExtension();
        $originalName = $f->getClientOriginalName();
        //$file_name = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1).'_'.time().'.'.$extension;
        $file_name = Library::rename_engine(substr($originalName, 0, 50), 1).'.'.$extension;
        $f->move($path, $file_name);

        return $file_name;
    }

    public function deleteFile($filename, $type='', $pathfile='') {
        $path = public_path()."/assets/".$type;
        if ($pathfile != '') {
            $path = $pathfile;
        }
        File::delete($path.'/'.$filename);
    }

    public static function rename_engine($content,$val,$mktime=1){

        if($val==1){
            $replace = array(" _ "," - "," : "," \" "," ' "," ` "," & "," ! "," , "," . ","_","-",":","\"","'","`","&","!",",","."," ");
            $delete  = array("#","$","%","(",")","*","+","/","\\","|",";","<",">","=","?","@","[","]","^","{","}","~","¡","¢","€","£","¤","¥","Ұ","₣","¦","§","¨","©","ª","«","¬","®","¯","°","±","²","³","´","µ","¶","•","¸","¹","º","»","¼","½","¾","¿","×","Ø","Ð","Þ","ß","÷","ø","ð","þ");
            $replace1 = array("À","Á","Â","Ã","Ä","Å","à","á","â","ã","ä","å");
            $replace2 = array("È","É","Ê","Ë","è","é","ê","ë");
            $replace3 = array("Ì","Í","Î","Ï","ì","í","î","ï");
            $replace4 = array("Ò","Ó","Ô","Õ","Ö","ò","ó","ô","õ","ö");
            $replace5 = array("Ù","Ú","Û","Ü","ù","ú","û","ü");
            $replace6 = array("Æ","Ç","Ñ","Ý","æ","ç","ñ","ý","ÿ");
            $replaceby6=array("ae","C","N","Y","ae","c","n","y","y");
            //
            $step =  str_replace($replace, "_", $content);
            $step1 = str_replace($delete, "", $step);
            $step2 = str_replace($replace1, "a", $step1);
            $step3 = str_replace($replace2, "e", $step2);
            $step4 = str_replace($replace3, "i", $step3);
            $step5 = str_replace($replace4, "o", $step4);
            $step6 = str_replace($replace5, "u", $step5);
            $step7 = str_replace($replace6, $replaceby6, $step6);
            $step8 = str_replace("____", "_", $step7);
            $step9 = str_replace("___", "_", $step8);
            $step10= str_replace("__", "_", $step9);
            //

            $add = "_".time();
            if($mktime==0){ $add = ""; }
            $returner = substr(strtolower(utf8_decode($step10)) ,0 ,80).$add;
        }
        if($val==0){
            //$returner = date("dmY")."e".date("His")."_".mktime();
            $returner = PIXER_ROOT."_".time()."e".date("His");
        }
        return $returner;

    }

    // function de miniaturisation
    public function resize($url_src,$url_dest,$taille_mini,$quality,$extension,$out,$filter_on,$tag_on,$square='',$height=''){
        $extension = strtolower($extension);
        $size = getimagesize($url_src);
        $scale = 1.2;//1.5 echelle photo standard 1200 x 800 :: 1.2 pour tendre vers une surface uniforme
        if ($size[0] >= $size[1]){$rapport = ($taille_mini / $size[0]);} //$taille_mini = taille maxi d'une mini
        else{$rapport = (($taille_mini/$scale) / $size[1]);}


        $put_middle = 0; $put_middle1 = 0;
        $dest_width = ceil($size[0] * $rapport);
        $dest_eight =  ceil($size[1] * $rapport);
        $value_width = $dest_width; $value_height = $dest_eight;

        if($height!=""){
            $dest_width = ceil($size[0] * $rapport);
            $dest_eight =  ceil($size[1] * $rapport);
            $value_width = $dest_width; $value_height = $height;//valeur de l'image a recréer
        }

        if($square!=''){
            if($size[0] >= $size[1]){ $rapport = ($taille_mini / $size[1]); }
            else{ $rapport = ($taille_mini / $size[0]); }
            $dest_width = ceil($size[0] * $rapport);
            $dest_eight =  ceil($size[1] * $rapport);
            $value_width = $taille_mini; $value_height = $taille_mini;
            //
            if($size[0] >= $size[1]){ $put_middle = ($taille_mini - $dest_width)/2; } else { $put_middle1 = ($taille_mini - $dest_eight)/10; }
        }



        /*if($extension=='gif'|| $extension=='GIF')$src_img = @imagecreatefromgif($url_src);
        elseif($extension=='png'|| $extension=='PNG')$src_img = imagecreatefrompng($url_src);
        elseif($extension=='jpg'|| $extension=='JPG' || $extension=='JPEG' || $extension=='jpeg') $src_img = @imagecreatefromjpeg($url_src);*/
        if($extension=='gif')$src_img = @imagecreatefromgif($url_src);
        elseif($extension=='png')$src_img = imagecreatefrompng($url_src);
        elseif($extension=='jpg' || $extension=='jpeg') $src_img = @imagecreatefromjpeg($url_src);

        //else {$mini_gdversion=15; echo 'image non valide';}

        /////////////////
        if($filter_on=='on' ){
            Library::put_filter($src_img,$src_img,PIXER_FILTER,$tag_on);
        }
        if($filter_on=='off'){
            Library::put_filter($src_img,$src_img,PIXER_FILTER,"off");
        }

        ///////////////////////
        $dst_img = imagecreatetruecolor($value_width,$value_height);
        if($out==2 && $extension=='png'){
            $dst_img = imagecreate($value_width,$value_height);
        }

        ////////////////////////
        if($extension=='png'){
            $couleur = imagecolorallocate($dst_img,0,0,0);
            imagecolortransparent($dst_img,$couleur);
        }
        //imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $dest_width, $dest_eight, $size[0], $size[1]);
        imagecopyresampled($dst_img, $src_img, $put_middle, $put_middle1, 0, 0, $dest_width, $dest_eight, $size[0], $size[1]);

        //touch si safe_mode=on pour essayer de creer les miniatures
        if (ini_get("safe_mode")){
            touch($url_dest);
        }
        if($out==1){
            // L'image est convertie dans son format d'origine
            if($extension=='gif'||$extension=='Gif')$rescreation=@imagegif($dst_img, $url_dest, $quality);
            elseif($extension=='png'||$extension=='PNG')$rescreation=@imagepng($dst_img, $url_dest);
            else
                $rescreation=@imagejpeg($dst_img, $url_dest, $quality);
        }
        if($out==0){
            // L'image est convertie dans le format jpg
            $rescreation=@imagejpeg($dst_img, $url_dest, $quality);
        }
        if($out==2){
            // L'image est convertie dans le format png
            $rescreation=@imagepng($dst_img, $url_dest);
        }

        if ($rescreation!=1) $rescreation=0;

        @imagedestroy($src_img);
        @imagedestroy($dst_img);
        return $rescreation;
    }

    //-----------------------------------------------------------
    //Applique a l'image le filtre selectionné ainsi que le tag
    public function put_filter($image,$image1,$appl_filtre,$tag_on){

        if($tag_on=="on"){

            $tager=asset('img/'.PIXER_TAG_FILE);
            $shaper_position=PIXER_TAG_POS;
            // Section pour le tag
            $w = imagesx($image);
            $h = imagesy($image);
            $tag = imagecreatefrompng($tager);
            $ww = imagesx($tag);
            $wh = imagesy($tag);

            switch($shaper_position){
                // left up corner
                case 1: $x_tag = 0; $y_tag = 0; break;
                // center up element
                case 2: $x_tag = ((imagesx($image))/2)-$ww/2; $y_tag = 0; break;
                // right up corner
                case 3: $x_tag = (imagesx($image))-$ww; $y_tag = 0; break;
                // left center corner
                case 4: $x_tag = 0;$y_tag = ((imagesy($image))/2)-$wh/2;break;
                // center element
                case 5: $x_tag = ((imagesx($image))/2)-$ww/2; $y_tag = ((imagesy($image))/2)-$wh/2; break;
                // right center corner
                case 6: $x_tag = (imagesx($image))-$ww; $y_tag = ((imagesy($image))/2)-$wh/2; break;
                // left down corner
                case 7: $x_tag = 0; $y_tag = (imagesy($image))-$wh; break;
                // center down element
                case 8: $x_tag = ((imagesx($image))/2)-$ww/2; $y_tag = (imagesy($image))-$wh; break;
                // right down corner
                case 9: $x_tag = (imagesx($image))-$ww; $y_tag = (imagesy($image))-$wh; break;
            }
            imagelayereffect($image, IMG_EFFECT_NORMAL);
            imagecopy($image, $tag, $x_tag, $y_tag, 0, 0, $ww, $wh);
        }

        /////////////////////////////////////////////////////////
        switch($appl_filtre){

            case 'filtre1': break;

            case 'filtre2':
                $image_w = imagesx($image);
                $image_h = imagesy($image);
                imagefilter($image,IMG_FILTER_CONTRAST,30);
                imagelayereffect($image, IMG_EFFECT_OVERLAY);
                imagefilter($image1, IMG_FILTER_GRAYSCALE);
                imagecopy($image, $image1, 0, 0, 0, 0, $image_w, $image_h);
                break;

            case 'filtre3':
                $image_w = imagesx($image);
                $image_h = imagesy($image);
                imagelayereffect($image, IMG_EFFECT_OVERLAY);
                imagefilter($image1, IMG_FILTER_GRAYSCALE);
                imagecopy($image, $image1, 0, 0, 0, 0, $image_w, $image_h);
                break;

            case 'filtre4':
                $image_w = imagesx($image);
                $image_h = imagesy($image);
                imagefilter($image,IMG_FILTER_CONTRAST, 40);
                imagefilter($image,IMG_FILTER_COLORIZE, -20, 0, 0);
                imagelayereffect($image, IMG_EFFECT_OVERLAY);
                imagefilter($image1, IMG_FILTER_GRAYSCALE);
                imagecopy($image, $image1, 0, 0, 0, 0, $image_w, $image_h);
                break;

            case 'filtre5':
                $image_w = imagesx($image);
                $image_h = imagesy($image);
                imagefilter($image,IMG_FILTER_CONTRAST, 40);
                imagefilter($image,IMG_FILTER_COLORIZE, -20, -20, 0);
                imagelayereffect($image, IMG_EFFECT_OVERLAY);
                imagefilter($image1, IMG_FILTER_GRAYSCALE);
                imagecopy($image, $image1, 0, 0, 0, 0, $image_w, $image_h);
                break;

            case 'filtre6':
                $image_w = imagesx($image);
                $image_h = imagesy($image);
                imagefilter($image,IMG_FILTER_CONTRAST, 40);
                imagefilter($image,IMG_FILTER_COLORIZE, -20, 0, -20);
                imagelayereffect($image, IMG_EFFECT_OVERLAY);
                imagefilter($image1, IMG_FILTER_GRAYSCALE);
                imagecopy($image, $image1, 0, 0, 0, 0, $image_w, $image_h);
                break;
        }
    }
    //-----------------------------------------------------------

    function remove_word($words,$count){
        $words_ok = "";
        $words = str_replace(","," ",$words);
        $words_explode = explode(" ",$words);
        //dd($words_explode);
        for($i=0;$i<count($words_explode);$i++){
            if(strlen($words_explode[$i])>$count){
                $words_ok .= $words_explode[$i]." ";
            }
        }
        return $words_ok;
    }
}
